﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TentacleMonster : DamageableActor
{
    public Rigidbody rb;
    public GameObject dropOnDestroy;

    // Start is called before the first frame update
    void Start()
    {
        rb.angularVelocity = Vector3.ClampMagnitude(new Vector3(0f, 0f, 2f), 2);
    }

    protected override void OnZeroHP()
    {
        var instance = Instantiate(dropOnDestroy, new Vector3(transform.position.x, 0f, transform.position.z), Quaternion.identity);
        Destroy(gameObject.transform.parent.gameObject);
    }

    public override void TakeDamage(float amt)
    {
        _HP_curr -= amt;
        if (_HP_curr <= 0f)
            OnZeroHP();
    }
}
