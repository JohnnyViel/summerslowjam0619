﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    public GameObject bulletPrefab;
    public Rigidbody _shipRB;
    [SerializeField, Range(0.2f, 10)] private float _bulletLifeTime = 2f; /**< should be in seconds, because it's independent of player velocity */
    [SerializeField] private float _forwardSpeed = 10f;
    [SerializeField] private float _bulletUnitWeight = 0.001f;
    [SerializeField, Range(0f, 1f)] private float _startDelay;
    [SerializeField, Range(0.001f, 0.5f)] private float _reloadDelay;
    private float _reloadTimer;
    [Range(0f, 30f)] public float bulletSpread;
    [SerializeField] private int _ammoCapacity;
    [SerializeField] private int _ammoLeft;
    [SerializeField] private AudioSource _shotSound;

    void Awake()
    {
        _reloadTimer = _reloadDelay - _startDelay;
        _shipRB = GetComponentInParent<Rigidbody>();
    }

    void Update()
    {
        //_reloadTimer += Time.deltaTime;
        if (Input.GetAxis(Platform.RT) > 0f)
            _reloadTimer += Time.deltaTime;
        else
            _reloadTimer = _reloadDelay - _startDelay;

        if (_reloadTimer >= _reloadDelay && Input.GetAxis(Platform.RT) > 0f){
            _reloadTimer = 0f;
            // TODO: Replace with pool
            var bullet = GameObject.Instantiate(bulletPrefab, transform.position, Quaternion.Euler(90f, transform.eulerAngles.y + (Random.value - 0.5f) * bulletSpread, 0f));
            var b = bullet.GetComponent<Bullet>();
            b.forwardSpeed = _forwardSpeed;
            b.lifeTime = _bulletLifeTime;
            bullet.GetComponent<Rigidbody>().velocity = _shipRB.velocity;
            GetComponentInParent<Rigidbody>().AddForce(-transform.forward * _forwardSpeed * _bulletUnitWeight, ForceMode.Impulse);
            _shotSound.Play();
        }
    }

    public void DecreaseReloadDelay(float amt)
    {
        _reloadDelay -= amt;
        if (_startDelay > 0)
            _startDelay = _reloadDelay / 2;
    }
}
