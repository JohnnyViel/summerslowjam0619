﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody),typeof(MeshCollider))]
public class Ice : DamageableActor
{
    private Rigidbody _rb;
    // Start is called before the first frame update
    void Awake()
    {
        _rb = GetComponent<Rigidbody>();
    }

    void Start()
    {
        float x = Random.Range(3, 6) * 100;
        transform.localScale = new Vector3(x, x, x);
        _rb.velocity = Vector3.ClampMagnitude(new Vector3(Random.Range(0,100), Random.Range(0,100), Random.Range(0,100)), 5f);
        _rb.angularVelocity = Vector3.ClampMagnitude(new Vector3(Random.Range(0,100), Random.Range(0,100), Random.Range(0,100)), 3f);
    }
}
