﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Asteroid : DamageableActor
{
    public bool original = true;
    public float maxSpeedRotate = 3f;
    public float maxSpeedMove;
    public float massScaleDownFactor;
    [Range (0.1f,0.9f)] public float breakSizeFactor;

    [SerializeField] private ParticleSystem _particles;
    private Rigidbody _rb;
    private float _volume;
    [SerializeField] private float _damagePerMassUnit;

#region DamageableActor
    public override void TakeDamage(float amt)
    {
        _particles.Play();
        _HP_curr -= amt;
        if (_HP_curr <= 0f)
            OnZeroHP();
    }

    protected override void OnZeroHP()
    {
        ScoreKeeper.Instance.AddToScore(AsteroidManager.Instance.pointsOnDestroy);
        if (_volume > AsteroidManager.Instance.minVolume){
            var dir = new Vector3(-1f, 0f, -1f);
            GenerateChunk(dir);
            dir = new Vector3(-1f, 0f, 1f);
            GenerateChunk(dir);
            dir = new Vector3(1f, 0f, 1f);
            GenerateChunk(dir);
        }
        DropLoot();
        StartCoroutine(ExplodeThenDestroy());
    }
#endregion

    void Awake()
    {
        _rb = GetComponent<Rigidbody>();
    }

    void Start()
    {
        _rb.angularVelocity = Vector3.ClampMagnitude(new Vector3(Random.Range(0,100), Random.Range(0,100), Random.Range(0,100)), maxSpeedRotate);
        _rb.velocity = _rb.velocity + Vector3.ClampMagnitude(new Vector3(Random.Range(0, 100), 0f, Random.Range(0, 100)), 3);
    }

    void Update()
    {
        if (Input.GetKeyDown(Platform.A)){
            _rb.angularVelocity = Vector3.ClampMagnitude(new Vector3(Random.Range(1,40), Random.Range(1,40), Random.Range(1,40)), maxSpeedRotate);
        }
    }

    void OnCollisionEnter (Collision col)
    {
        if (col.gameObject.tag == tag) return; // Asteroids should not damage each other

        var damageable = col.gameObject.GetComponent<DamageableActor>();
        damageable?.TakeDamage(_damagePerMassUnit * col.relativeVelocity.magnitude);
    }

    public void Resize(float factor, float refVolume = 64000000)
    {
        float x = Mathf.Pow(refVolume, 1f/3f);
        transform.localScale = new Vector3(x * factor, x * factor, x * factor);
        _volume = Mathf.Pow(x * factor, 3);
        _rb.mass = _volume * massScaleDownFactor;
    }

    void GenerateChunk(Vector3 dir)
    {
        var obj = Instantiate(AsteroidManager.Instance.asteroidPrefab, transform.position + dir, Quaternion.identity);
        var ast = obj.GetComponent<Asteroid>();
        var rb = obj.GetComponent<Rigidbody>();
        ast.Resize(breakSizeFactor, _volume);
        ast.original = false;
        ast.HP_max = this.HP_max;
        ast.Heal(HP_max);
        //rb.velocity = _rb.velocity;
        //rb.AddForce(transform.position - dir * AsteroidManager.Instance.breakForce * Random.Range(0.5f,1f), ForceMode.Force);
    }

    void DropLoot()
    {
        // TODO: Calculate likelihood first
        var obj = LootManager.Instance.GenerateLoot();
        if (!obj) return;
        var instance = Instantiate(obj, transform.position, Quaternion.identity);
        var s = obj.GetComponent<SellablePickup>();
        if (s)
            s.count = (int)Mathf.Clamp((_rb.mass * 50), 1, Mathf.Infinity);
    }

    private IEnumerator ExplodeThenDestroy()
    {
        var rend = GetComponent<MeshRenderer>();
        var col = GetComponent<Collider>();
        rend.enabled = false;
        col.enabled = false;
        _particles.Play();
        yield return new WaitForSeconds(1f);
        if (original){
            rend.enabled = true;
            col.enabled = true;
            AsteroidSpawner.Instance.RecycleAsteroid(this);
        }
        else
            Destroy(gameObject);
    }
}
