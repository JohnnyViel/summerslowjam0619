﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

#if UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
using WellFired.Command.Unity.Runtime.CommandHandlers;
using WellFired.Command.Unity.Runtime.Console;
using Debug = WellFired.Command.Log.Debug;
#endif

/**
 * Singleton, persistent for entire app runtime
 */
public class GameManager : MonoBehaviour
{
    public static GameManager Instance = null;
    private int _levelToLoad = 0;

    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this){
            Destroy(gameObject);
            return;
        }

#if UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
        // App specifics
        Application.targetFrameRate = 60; // required to make 60fps on mobile

        if (UnityEngine.Debug.isDebugBuild){
            // Loads .Command with the default settings.
            DevelopmentConsole.Load();
            // Register built in .Command Inspect commands
            DevelopmentCommands.Register(typeof(Inspect));
            // Show the 'show .Command' for quick access if users don't have a tilde key (for instance mobile platforms)
            DevelopmentConsole.Instance.DrawShowDotCommandButton = true;
            // Set a special message on the 'show .Command' button
            DevelopmentConsole.Instance.ShowDotCommandButtonMessage = "(~)";
            // Tell the 'open console' button to display in the top left corner
            DevelopmentConsole.Instance.DisplayCorner = DisplayCorner.TopLeft;
            // Get a notification when the visible state of the console changes. I.E. It becomes visible
            DevelopmentConsole.Instance.VisibleStateChange += (visible) => {
                if(visible){
                    // Disable In-Game Controls, etc.
                } else {
                    // Enable in-Game Controls
                }
            };
        }
#endif

        SceneManager.sceneLoaded += (scene, mode) => {
        };
    }

    public void mainMenu()
    {
        unPause();
        loadScene(0);
    }

    public void loadScene()
    {
        SceneManager.LoadScene(_levelToLoad);
        unPause();
    }

    public void loadScene(int buildIndex)
    {
        _levelToLoad = buildIndex;
    }

    public void loadNextScene()
    {
        _levelToLoad = SceneManager.GetActiveScene().buildIndex + 1;
    }

    public void restartLevel()
    {
        _levelToLoad = SceneManager.GetActiveScene().buildIndex;
        PauseMenuBehavior.pausePressed -= GameOver;
        SceneManager.LoadScene(_levelToLoad);
    }

    public void pause()
    {
        Time.timeScale = 0f;
    }

    public void unPause()
    {
        Time.timeScale = 1f;
    }

    public void ShowEndScreen()
    {
        PauseMenuBehavior.Instance.clickedPause();
        PauseMenuBehavior.pausePressed += GameOver;
    }

    public void GameOver()
    {
        restartLevel();
    }
}
