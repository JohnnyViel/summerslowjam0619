﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessageScroller : MonoBehaviour
{
    public static MessageScroller Instance = null;
    [Range(0.01f,1)] public float transparency;

    public GameObject textObject, chatPanel;
    public int maxMessageNumber = 25;

    public List<GameObject> messages = new List<GameObject>();

    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this){
            Destroy(gameObject);
            return;
        }
    }

    // void Update()
    // {
    //     if (Input.GetKeyDown(KeyCode.Space))
    //         DisplayMessage("you pressed space!", Color.red);
    // }

    public void Clear()
    {
        foreach (var msg in messages)
            Destroy(msg);
        messages.Clear();
    }

    public void DisplayMessage(string msg, Color col)
    {
        if (messages.Count > maxMessageNumber){
            var m = messages[messages.Count-1];
            messages.RemoveRange(messages.Count-1,1);
            Destroy(m);
        }

        var text = Instantiate(textObject, chatPanel.transform).GetComponent<Text>();
        text.GetComponent<RectTransform>().SetAsFirstSibling();
        text.text = msg;
        col.a = transparency;
        text.color = col;

        messages.Insert(0, text.gameObject);
    }
}