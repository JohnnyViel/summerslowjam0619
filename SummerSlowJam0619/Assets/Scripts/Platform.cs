﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** Platform specific strings that map to controls etc. */
public static class Platform
{
#if UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
    public static readonly string RT = "RT_mac";
    public static readonly string LT = "LT_mac";
    public static readonly string LSH = "LSH_mac";
    public static readonly string LSV = "LSV_mac";
    public static readonly string RSH = "RSH_mac";
    public static readonly string RSV = "RSV_mac";
    public static readonly string RB = "joystick button 14";
    public static readonly string LB = "joystick button 13";
    public static readonly string START = "joystick button 9";
    public static readonly string SELECT = "joystick button 10";
    public static readonly string A = "joystick button 16";
    public static readonly string B = "joystick button 17";
    public static readonly string X = "joystick button 18";
    public static readonly string Y = "joystick button 19";
    public static readonly float TRIGGER_MIN_VAL = -1f;
    public static readonly float TRIGGER_MAX_VAL = 1f;
    public static readonly float TRIGGER_RANGE = TRIGGER_MAX_VAL - TRIGGER_MIN_VAL;

#elif UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
    public static readonly string RT = "RT_win";
    public static readonly string LT = "LT_win";
    public static readonly string LSH = "LSH_win";
    public static readonly string LSV = "LSV_win";
    public static readonly string RSH = "RSH_win";
    public static readonly string RSV = "RSV_win";
    public static readonly string RB = "joystick button 5";
    public static readonly string LB = "joystick button 4";
    public static readonly string START = "joystick button 7";
    public static readonly string SELECT = "joystick button 6";
    public static readonly string A = "joystick button 0";
    public static readonly string B = "joystick button 1";
    public static readonly string X = "joystick button 2";
    public static readonly string Y = "joystick button 3";
    public static readonly float TRIGGER_MIN_VAL = 0f;
    public static readonly float TRIGGER_MAX_VAL = 1f;
    public static readonly float TRIGGER_RANGE = TRIGGER_MAX_VAL - TRIGGER_MIN_VAL;

#endif

}
