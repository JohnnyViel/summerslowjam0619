﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider), typeof(AsteroidManager))]
public class AsteroidSpawner : MonoBehaviour
{
    public static AsteroidSpawner Instance = null;

    public Rigidbody ship_RB;
    public BoxCollider boxCol;
    
    private AsteroidManager _asteroidManager;
    [SerializeField] private float _spawnerWidth = 300f;
    [SerializeField] private float _spawnerHeight = 300f;
    private Vector3 _offSetFromShip;
    [SerializeField, Range(0.05f, 1)] private float _spawnerPercentOfWidth;
    [SerializeField] private int _maxActiveAsteroids;
    [SerializeField] private int _currentActiveAsteroids;
    [SerializeField, Tooltip("min change of velocity angle to update active spawner(s)")]
    private float _updateSensitivity;
    [SerializeField] private float _distBetweenSpawns;
    [SerializeField] private int _asteroidsPerSpawn;

    private Vector3 _currVelocity;
    private float _distanceSinceLastSpawn;

    private spawnArea spawnU, spawnR, spawnD, spawnL;
    private List<spawnArea> activeSpawners;

    private class spawnArea{
        public float minX, maxX, minZ, maxZ;
        public spawnArea(float minx, float maxx, float minz, float maxz){
            minX = minx; maxX = maxx; minZ = minz; maxZ = maxz;
        }
    }

    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this){
            Destroy(gameObject);
            return;
        }

        Vector3 sz = new Vector3(_spawnerWidth, 5f, _spawnerHeight);
        boxCol.size = sz;
        float percent = 1 - _spawnerPercentOfWidth;
        spawnU = new spawnArea(0f, sz.x, sz.z * percent, sz.z);
        spawnR = new spawnArea(sz.x * percent, sz.x, 0f, sz.z);
        spawnD = new spawnArea(0, sz.x, 0f, sz.z * _spawnerPercentOfWidth);
        spawnL = new spawnArea(0, sz.x * _spawnerPercentOfWidth, 0f, sz.z);

        activeSpawners = new List<spawnArea>();
        activeSpawners.Add(spawnU);
        activeSpawners.Add(spawnR);
        activeSpawners.Add(spawnD);
        activeSpawners.Add(spawnL);

        _offSetFromShip = transform.position - new Vector3(sz.x / 2, 0f, sz.z / 2);
        boxCol.center = ship_RB.transform.position - _offSetFromShip;
    }

    void Start()
    {
        _asteroidManager = AsteroidManager.Instance;
        _currVelocity = ship_RB.velocity;
        UpdateActiveSpawners();
        SpawnAsteroids(25);
    }

    void Update()
    {
#if UNITY_EDITOR
        DrawSpawnAreas();
#endif
        transform.position = ship_RB.transform.position + _offSetFromShip;
        _distanceSinceLastSpawn += ship_RB.velocity.magnitude * Time.deltaTime;

        if (_currentActiveAsteroids >= _maxActiveAsteroids) return;
        if (_distanceSinceLastSpawn < _distBetweenSpawns) return;

        // check if rb has changed velocity much
        float angleDelta = Vector3.Angle(ship_RB.velocity, _currVelocity);
        if (angleDelta > _updateSensitivity)
            UpdateActiveSpawners();
        
        SpawnAsteroids(_asteroidsPerSpawn);
    }

    void OnTriggerExit(Collider other)
    {
        var a = other.GetComponent<Asteroid>();
        if (a){
            if (a.original){
                _asteroidManager.RecycleAsteroid(a);
                _currentActiveAsteroids--;
            }
            else
                Destroy(a.gameObject);
        }
        var p = other.GetComponent<Pickup>();
        if (p)
            Destroy(p.gameObject);
    }

    private void DrawSpawnAreas(){
        var sz = boxCol.size;
        Debug.DrawLine(transform.position + new Vector3(sz.x * _spawnerPercentOfWidth, 0f, 0f), transform.position + new Vector3(sz.x * _spawnerPercentOfWidth, 0f, sz.z), Color.cyan);
        Debug.DrawLine(transform.position + new Vector3(0f, 0f, sz.z * _spawnerPercentOfWidth), transform.position + new Vector3(sz.x, 0f, sz.z * _spawnerPercentOfWidth), Color.cyan);
    }

    private void UpdateActiveSpawners()
    {
        activeSpawners.Clear();
        _currVelocity = (ship_RB.velocity == Vector3.zero) ? Vector3.forward : ship_RB.velocity;
        var up = Vector3.Angle(_currVelocity, Vector3.forward);
        var right = Vector3.Angle(_currVelocity, Vector3.right);
        var down = Vector3.Angle(_currVelocity, Vector3.back);
        var left = Vector3.Angle(_currVelocity, Vector3.left);
        float minAngle = Mathf.Min(up,right,left,down);

        /* purposely adds all 4 if velocity is zero */
        if (minAngle == up)
            activeSpawners.Add(spawnU);
        if (minAngle == right)
            activeSpawners.Add(spawnR);
        if (minAngle == down)
            activeSpawners.Add(spawnD);
        if (minAngle == left)
            activeSpawners.Add(spawnL);
    }

    public void SpawnAsteroids(int num)
    {
        int mod = activeSpawners.Count;
        for (int spawned = 0; spawned < num; ++spawned)
        {
            var spawner = activeSpawners[spawned % mod];
            Vector3 off = new Vector3(Random.Range(spawner.minX, spawner.maxX), 0f, Random.Range(spawner.minZ, spawner.maxZ));
            Vector3 pos = transform.position + off;
            var ast = AsteroidManager.Instance.GetAsteroid();
            ast.transform.position = pos;
            _currentActiveAsteroids++;
        }
        _distanceSinceLastSpawn = 0f;
    }

    public void RecycleAsteroid(Asteroid a)
    {
        _asteroidManager.RecycleAsteroid(a);
        _currentActiveAsteroids--;
    }
}
