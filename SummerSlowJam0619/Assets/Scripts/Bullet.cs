﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Bullet : MonoBehaviour
{
    public float lifeTime; /**< should be in seconds, because it's independent of player velocity */
    private float _timeAlive = 0f;
    public float forwardSpeed;
    public float damage;

    void Update()
    {
        _timeAlive += Time.deltaTime;
        transform.Translate((transform.forward * -forwardSpeed) * Time.deltaTime);
        if (_timeAlive >= lifeTime)
            GameObject.Destroy(gameObject);
    }

    void OnCollisionEnter(Collision col)
    {
        var damageable = col.gameObject.GetComponent<DamageableActor>();
        damageable?.TakeDamage(damage);
        GameObject.Destroy(gameObject);
    }
}
