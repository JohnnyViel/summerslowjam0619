﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Pickup : MonoBehaviour
{
    public int count = 1;
    public int pointsPerUnit = 10;
    public int pointsOnPickup = 5;
    public float weightPerUnit = 1;
    public string descriptor; // shown when picked up
    public Color messageColor;

    public virtual void OnAwake(){
        var rb = GetComponent<Rigidbody>();
        rb.angularVelocity = Vector3.ClampMagnitude(new Vector3(Random.Range(0,100), Random.Range(0,100), Random.Range(0,100)), 3f);
    }

    protected virtual void OnAppear() { Debug.Log(gameObject + " has not implemented OnAppear() yet!"); }
    protected virtual void OnPickUp() { Debug.Log(gameObject + " has not implemented OnAppear() yet!"); }

    void OnCollisionEnter(Collision col)
    {
        if (col.collider.gameObject.tag.Equals("Player")){
            var sc = col.collider.GetComponent<ShipController>();
            sc.AddToCargo(gameObject);
            sc.AddPoints(pointsOnPickup);
            OnPickUp();
        }
    }
}
