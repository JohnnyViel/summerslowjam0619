﻿using UnityEngine;

public class SellablePickup : Pickup
{
    public float pricePer = 1;

#region Pickup
    protected override void OnAppear() { 

    }
    protected override void OnPickUp() { 
        MessageScroller.Instance.DisplayMessage("+ " + count + " " + descriptor, messageColor);
        Destroy(gameObject);
    }
#endregion

    public void OnSell() {}

    void Awake(){
        OnAwake();
    }

    // void OnCollisionEnter(Collision col)
    // {
    //     var sc = col.collider.GetComponent<ShipController>();
    //     sc?.AddToCargo(col.gameObject);
    //     sc?.AddPoints(10);
    //     if (col.collider.gameObject.tag.Equals("Player")){
    //         OnPickUp();
    //     }
    // }
}
