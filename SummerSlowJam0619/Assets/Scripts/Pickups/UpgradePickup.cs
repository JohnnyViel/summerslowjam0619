﻿using UnityEngine;

public class UpgradePickup : Pickup
{
    public StatType stat;
    public float bonusToStat;
    public GameObject coordinateObject = null; /**< only for coordinate type */

    public enum StatType {
        HEALTH,
        BULLET_RATE,
        BULLET_DAMAGE,
        ENGINE_POWER,
        ENGINE_COOLING,
        COORDINATE, /**< Gives coordinate for a great upgrade item */
    }

#region Pickup
    protected override void OnAppear() { 

    }
    protected override void OnPickUp() { 
        string msg = "+ " + count + " " + descriptor;    
        MessageScroller.Instance.DisplayMessage(msg, messageColor);
        Destroy(gameObject);
    }
#endregion

    void Awake(){
        OnAwake();
    }
    void Start(){
        if (stat == StatType.COORDINATE){
            coordinateObject = LootManager.Instance.GetMajorUpgrade();
        }
    }
}
