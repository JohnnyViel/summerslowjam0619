﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundTiler : MonoBehaviour
{
    public Rigidbody _player_RB;
    public Renderer _rend;

    [Range(0.00003f,0.01f)] public float _panSpeed;

    void Update()
    {
        transform.position = new Vector3(_player_RB.transform.position.x, transform.position.y, _player_RB.transform.position.z);
        _rend.material.mainTextureOffset -= new Vector2(_player_RB.velocity.x, _player_RB.velocity.z) * _panSpeed * Time.deltaTime;
    }
}
