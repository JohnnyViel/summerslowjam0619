﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Rigidbody))]
public class ShipController : DamageableActor
{
    public float fuelLevel = 100f;
    public float engineTemp = 0f;
    public float engineHeatRate = 5f;
    public float boostHeatRate;
    public float engineCoolRate = 3f;
    public float engineMaxTemp = 1000f;
    public float maxSpeedDriftToStop = 5f;
    private bool _engineOverheated = false;
    [SerializeField] private Slider _healthSlider;
    [SerializeField] private Slider _engineTempSlider;
    [SerializeField] private Image _tempFill;
    [SerializeField] private Gun _gunL;
    [SerializeField] private Gun _gunR;
    public float baseEnginePower = 1f;

    public Text velocityText;
    public Text massText;
    public Text positionText;

    public GameObject thrusters;
    private Vector3 _thrusterScaleDefault;
    public GameObject ionEngine;
    private Vector3 _ionEngineScaleDefault;

    private Vector3 _velocityLastFrame = Vector3.zero;

    private Rigidbody _rb;
    [SerializeField] private float _mainThrusterPowerFactor = 2f; /**< Power bonus while using main thruster */
    private float _power;
    private Vector3 _powerVector = Vector3.zero;
    private Vector3 _lookVector = Vector3.zero;
    private Vector3 _lookTarget = Vector3.zero;
    private float _mainEnginePower = 0f;
    [SerializeField, Range(0,1)] private float _lookDeadZone;
    [SerializeField, Range(0,15)] private float _rotateSpeed = 1f;
    private IEnumerator rotateCoroutine = null;

    public Text coordinatesText;
    private int _pointsInCargo = 0;

    public Dictionary<string, GameObject> POIs = new Dictionary<string, GameObject>();

#region DamageableActor
    public override void Heal(float amt){
        _HP_curr += amt;
        _healthSlider.value = _HP_curr;
    }

    public override void TakeDamage(float amt)
    {
        _HP_curr -= amt;
        _healthSlider.value = _HP_curr;
        // play anim or noise
        if (_HP_curr <= 0f)
            OnZeroHP();
    }

    protected override void OnZeroHP()
    {
        GameManager.Instance.ShowEndScreen();
    }
#endregion

    void Awake()
    {
        _HP_curr = HP_max;
        _rb = GetComponent<Rigidbody>();
        _thrusterScaleDefault = thrusters.transform.localScale;
        _ionEngineScaleDefault = ionEngine.transform.localScale;
        _engineTempSlider.maxValue = engineMaxTemp;
        _healthSlider.value = _healthSlider.maxValue = _HP_curr;
        _engineTempSlider.value = 0f;

        // fixes X/Z rotation bug for some reason. "Freeze Rotation" checkbox in Rigidbody component does not work right without this
        _rb.inertiaTensor = new Vector3(10000, _rb.inertiaTensor.y, 10000);
        _rb.inertiaTensorRotation = Quaternion.identity;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        GetInput();

        if (engineTemp > 0f)
            engineTemp -= engineCoolRate * Time.fixedDeltaTime;
        else{
            engineTemp = 0f;
            _engineOverheated = false;
        }

        /* LOOK DIRECTION */
        if (_lookVector.magnitude > 0f){
            _rb.angularVelocity = Vector3.zero;
            _lookTarget = _lookVector;
        } else if (_powerVector.magnitude > 0) {
            _rb.angularVelocity = Vector3.zero;
            _lookTarget = _powerVector.normalized;
        }

        float rotationDelta = Vector3.SignedAngle(transform.forward, _lookTarget, Vector3.up);
        transform.Rotate(Vector3.up, rotationDelta * _rotateSpeed * Time.fixedDeltaTime, Space.Self);

        /* Slow to full stop if less than 5 m/s */
        if (_rb.velocity.magnitude < maxSpeedDriftToStop && _rb.velocity.magnitude > 0.005f && _powerVector.magnitude == 0 && _mainEnginePower <= 0){
            _rb.velocity = Vector3.Lerp(_rb.velocity, Vector3.zero, 1 * Time.fixedDeltaTime);
        }

        UpdateCluster();

        /* ENGINE STUFF */
        if (_engineOverheated || fuelLevel <= 0f) return;

        // prioritize main engine
        if (_mainEnginePower > 0f)
        {
            ionEngine.SetActive(true);
            ionEngine.transform.localScale = new Vector3(_ionEngineScaleDefault.x, _mainEnginePower * _ionEngineScaleDefault.y, _ionEngineScaleDefault.z);
            _rb.AddForce(baseEnginePower * _mainThrusterPowerFactor * transform.forward * Time.fixedDeltaTime, ForceMode.Force);
            engineTemp +=(baseEnginePower * _mainThrusterPowerFactor * _mainEnginePower * boostHeatRate * Time.fixedDeltaTime);
            CheckEngineOverheat();
        }
        else{
            ionEngine.SetActive(false);
        }

        if (_engineOverheated || fuelLevel <= 0f) return;

        // secondary thrusters
        if (_powerVector.magnitude > 0f)
        {
            // Thruster visual update
            thrusters.SetActive(true);
            thrusters.transform.localScale = new Vector3(_thrusterScaleDefault.x, _thrusterScaleDefault.y, _powerVector.magnitude * _thrusterScaleDefault.z);
            thrusters.transform.forward = -_powerVector;

            _rb.AddForce(baseEnginePower * _powerVector * Time.fixedDeltaTime, ForceMode.Force);
            engineTemp +=(baseEnginePower * _powerVector.magnitude * engineHeatRate * Time.fixedDeltaTime);
            CheckEngineOverheat();
        } 
        else{
            thrusters.SetActive(false);
        }
    }

    private void CheckEngineOverheat()
    {
        if (engineTemp >= engineMaxTemp){
            ionEngine.SetActive(false);
            thrusters.SetActive(false);
            _engineOverheated = true;
            StartCoroutine(FlashTempBar());
        }
    }

    private bool MouseOnScreen()
    {
        Vector2 mousePos = Input.mousePosition;
        return (mousePos.x >= 0f) && (mousePos.x <= Screen.width) && (mousePos.y >= 0f) && (mousePos.y <= Screen.height);
    }

    void GetInput()
    {
        float lh, lv, rh, rv, lt;
        lh = Input.GetAxis(Platform.LSH);
        lv = -Input.GetAxis(Platform.LSV);
        rh = Input.GetAxis(Platform.RSH);
        rv = -Input.GetAxis(Platform.RSV);
        lt = Input.GetAxis(Platform.LT);
        // TODO: figure out how to use full [-1,1] range for triggers, not just [0,1]

        _powerVector = Vector3.ClampMagnitude(new Vector3(lh, 0f, lv), 1);
        _lookVector = Vector3.ClampMagnitude(new Vector3(rh, 0f, rv), 1);
        _mainEnginePower = lt;
        if (_lookVector.magnitude < _lookDeadZone)
            _lookVector = Vector3.zero;
        else
            _lookVector.Normalize();
    }

    private IEnumerator RotateToward(Vector3 lookTarget)
    {
        // Accelerate to full rotate speed or half-way, whichever comes first, then decelerate.

        float rotationDelta = Vector3.SignedAngle(transform.forward, lookTarget, Vector3.up);
        float iterations = (Mathf.Abs(rotationDelta) / 180) * _rotateSpeed;
        while (transform.forward != lookTarget){
            float step = 0.5f + 0.5f * Mathf.Sin(rotationDelta - Mathf.PI/2);
            //(Camera.main.ScreenToWorldPoint(Input.mousePosition - CameraFollow.Instance.offset) - transform.position).normalized
            rotationDelta -= step;
            transform.Rotate(Vector3.up, step * _rotateSpeed * Time.deltaTime);
            yield return null;
        }
    }

    private void UpdateCluster()
    {
        string s = _rb.velocity.magnitude.ToString("F2") + " m/s";
        _velocityLastFrame = _rb.velocity;
        _engineTempSlider.value = Mathf.Clamp(engineTemp, 0f, _engineTempSlider.maxValue);        
        velocityText.text = s;
        massText.text = _rb.mass.ToString() + " kt";
        positionText.text = "( " + transform.position.x.ToString("F0") + " , " + transform.position.z.ToString("F0") + " )";
    }

    public void AddPoints(int amt)
    {
        ScoreKeeper.Instance.AddToScore(amt);
    }

    public void AddToCargo(GameObject obj)
    {
        SellablePickup s = obj.GetComponent<SellablePickup>();
        if (s){
            _rb.mass += s.count * s.weightPerUnit;
            _pointsInCargo += s.count * s.pointsPerUnit;
            return;
        }

        UpgradePickup u = obj.GetComponent<UpgradePickup>();
        if (u){
            switch(u.stat)
            {
            case UpgradePickup.StatType.HEALTH:
                Heal(u.bonusToStat);
                MessageScroller.Instance.DisplayMessage((u.bonusToStat).ToString() + " shield recharged", Color.cyan);
                break;
            case UpgradePickup.StatType.BULLET_RATE:
                _gunL.DecreaseReloadDelay(u.bonusToStat);
                _gunR.DecreaseReloadDelay(u.bonusToStat);
                MessageScroller.Instance.DisplayMessage("Increased reload speed by " + (-u.bonusToStat).ToString() + " seconds!", Color.cyan);
                break;
            case UpgradePickup.StatType.BULLET_DAMAGE:
                break;
            case UpgradePickup.StatType.ENGINE_POWER:
                break;
            case UpgradePickup.StatType.ENGINE_COOLING:
                engineCoolRate += u.bonusToStat;
                MessageScroller.Instance.DisplayMessage("Engine cooling up by " + (u.bonusToStat).ToString() + "C per second!", Color.cyan);
                break;
            case UpgradePickup.StatType.COORDINATE:
                MessageScroller.Instance.DisplayMessage("Receiving signal ... coordinate added", Color.cyan);
                string coord = "? ("  + u.coordinateObject.transform.position.x.ToString() 
                                + "," + u.coordinateObject.transform.position.z.ToString() + ")";
                //POIs.Add(coord, u.coordinateObject);
                coordinatesText.text = coordinatesText.text + "\n" + coord;
                break;
            default:
                Debug.Log(u.stat + " upgrade not implemented yet");
                break;
            }
        }
    }

    public void UnloadCargo()
    {
        Debug.Log("Unloading");
        _rb.mass = 1;
        if (_pointsInCargo == 0) return;

        AddPoints(_pointsInCargo);
        MessageScroller.Instance.Clear();
        MessageScroller.Instance.DisplayMessage("Sold cargo for " + _pointsInCargo + " points!", Color.cyan);
        _pointsInCargo = 0;
    }

    private IEnumerator FlashTempBar()
    {
        float timer = 0f;
        Color c = Color.red;
        while (engineTemp > 0f)
        {
            c.r = 255 * (1 + Mathf.Sin(timer * 2));
            _tempFill.color = c;
            timer += Time.deltaTime;
            yield return null;
        }
        _tempFill.color = Color.blue;
    }
}
