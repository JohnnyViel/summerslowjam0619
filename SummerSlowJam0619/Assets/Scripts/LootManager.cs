﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootManager : MonoBehaviour
{
    public static LootManager Instance = null;
    public Transform playerTrans;

    public List<GameObject> tier1 = new List<GameObject>();
    public List<GameObject> tier2 = new List<GameObject>();
    public List<GameObject> tier3 = new List<GameObject>();

    public List<GameObject> majorUpgrades = new List<GameObject>();

    private int idx = 0;

    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this){
            Destroy(gameObject);
            return;
        }
        if (!playerTrans)
            playerTrans = GameObject.Find("Player").GetComponent<Transform>();
    }

    public GameObject GenerateLoot()
    {
        float mod = (playerTrans.position.magnitude/100);
        float val = Random.Range(0 + mod, 100);
        Debug.Log(mod + ", " + val);
        
        List<GameObject> bucket;
        if (val < 60){
            bucket = null;
        }
        else if (val < 70){
            bucket = tier1;
        }
        else if (val <  95)
            bucket = tier2;
        else{
            bucket = tier3;
        }
        if (bucket != null)
            return bucket[Random.Range(0, bucket.Count)];
        return null;
    }

    public GameObject GetMajorUpgrade()
    {
        return majorUpgrades[idx++ % majorUpgrades.Count];
    }
}
