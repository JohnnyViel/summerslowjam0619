﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class DamageableActor : MonoBehaviour
{
    [SerializeField] public float HP_max;
    public float _HP_curr;
    public virtual void Heal(float amt){ _HP_curr = Mathf.Clamp(_HP_curr + amt, 0f, HP_max); }
    public virtual void TakeDamage(float amt){ Debug.Log(gameObject + " has not implemented DamageableActor.TakeDamage() yet"); }
    protected virtual void OnZeroHP(){ Debug.Log(gameObject + " has not implemented DamageableActor.OnZeroHP() yet"); }

    void Awake(){
        _HP_curr = HP_max;
    }
}
