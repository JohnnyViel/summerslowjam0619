﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreKeeper : MonoBehaviour
{
    public static ScoreKeeper Instance = null;

    public int score { get; private set; }

    [SerializeField] private Text _scoreText;
    [SerializeField] private Text _finalScoreText;
    private readonly string _prefix = "Score: ";

    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this){
            Destroy(gameObject);
            return;
        }
        Reset();
    }

    public void AddToScore(int amt)
    {
        score += amt;
        _scoreText.text = _prefix + score.ToString();
        _finalScoreText.text = _prefix + score.ToString();
    }

    public void Reset()
    {
        score = 0;
        _scoreText.text = _prefix + score.ToString();
    }
}
