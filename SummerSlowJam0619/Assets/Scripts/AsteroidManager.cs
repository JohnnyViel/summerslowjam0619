﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidManager : MonoBehaviour
{
    public static AsteroidManager Instance = null;
    public GameObject asteroidPrefab;
    public float minVolume;
    public Vector3 defaultScale = new Vector3(400,400,400);
    public float breakForce; /**< how much force is applied to subdivisions after destroying main body */
    public List<Mesh> meshes;
    public List<Material> materials;

    public int pointsOnDestroy;
    public Queue<GameObject> asteroidPool;

    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this){
            Destroy(gameObject);
            return;
        }
        asteroidPool = new Queue<GameObject>();
        GenerateAsteroids(100);
    }

    public Asteroid GetAsteroid()
    {
        if(asteroidPool.Count == 0)
            GenerateAsteroids(100);

        var a = asteroidPool.Dequeue();
        a.SetActive(true);
        return a.GetComponent<Asteroid>();
    }

    public void RecycleAsteroid(Asteroid a)
    {
        a.gameObject.SetActive(false);
        asteroidPool.Enqueue(a.gameObject);
    }

    private void GenerateAsteroids(int num)
    {
        for (int i = 0; i < num; i++){
            GameObject asteroid = Instantiate(asteroidPrefab, Vector3.zero, Quaternion.identity);
            asteroid.GetComponent<Asteroid>().Resize(1);

            /* Mesh */
            int mesh = Random.Range(0,6);
            asteroid.GetComponent<MeshFilter>().mesh = meshes[mesh];

            /* Material */
            int material = Random.Range(0,3);
            Material [] mats = new Material[1];
            mats[0] = materials[material];
            asteroid.GetComponent<MeshRenderer>().materials = mats;

            asteroid.SetActive(false);
            asteroidPool.Enqueue(asteroid);
        }
    }
}
