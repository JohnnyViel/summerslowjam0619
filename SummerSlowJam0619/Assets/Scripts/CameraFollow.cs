﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public static CameraFollow Instance = null;
    public Transform target;
    public Vector3 offset;
    public float maxFollowDistance;
    [SerializeField, Range(0.3f,5)] private float _smoothing = 1f;


    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this){
            Destroy(gameObject);
            return;
        }

        if (!target)
            target = GameObject.Find("Ship").GetComponent<Transform>();
        offset = transform.position - target.position;
    }

    void FixedUpdate()
    {
        Vector3 targetPos = target.position;
        Vector3 dest = Vector3.Lerp(transform.position - offset, targetPos, _smoothing * Time.deltaTime);
        if ((targetPos - dest).magnitude > maxFollowDistance){
            Vector3 off = Vector3.ClampMagnitude(targetPos - dest, maxFollowDistance);
            dest = targetPos - off;
        }
        transform.position = dest + offset;
        //if (Input.GetButtonDown(
    }
}
