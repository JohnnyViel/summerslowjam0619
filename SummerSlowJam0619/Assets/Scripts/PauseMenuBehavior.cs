﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenuBehavior : MonoBehaviour
{
    public static PauseMenuBehavior Instance = null;
    public List<GameObject> disableOnPause;
    public GameObject pauseMenu;

    public delegate void PauseEvent();
    public static PauseEvent pausePressed; 

    private bool _paused = false;

    void Awake(){
        if (Instance == null)
            Instance = this;
        else if (Instance != this){
            Destroy(gameObject);
            return;
        }
        pausePressed += () => {};
    }
    // Start is called before the first frame update
    void Start()
    {
        foreach (GameObject icon in disableOnPause)
            icon.SetActive(true);
        pauseMenu.SetActive(false);
        clickedPause();
    }

    void Update()
    {
        if (Input.GetKeyDown(Platform.START)){
            if (_paused){
                clickedUnpause();
                pausePressed();
            }
            else
                clickedPause();
        }
    }

    public void toggleMusic()
    {
    }

    public void clickedReload()
    {
        GameManager.Instance.unPause();
        GameManager.Instance.restartLevel();
    }

    public void clickedPause()
    {
        foreach (GameObject icon in disableOnPause)
            icon.SetActive(false);
        pauseMenu.SetActive(true);
        GameManager.Instance.pause();
        _paused = true;
    }

    public void clickedUnpause()
    {
        foreach (GameObject icon in disableOnPause)
            icon.SetActive(true);
        pauseMenu.SetActive(false);
        GameManager.Instance.unPause();
        _paused = false;
    }

    public void clickedExit()
    {
        GameManager.Instance.mainMenu();
    }
}
