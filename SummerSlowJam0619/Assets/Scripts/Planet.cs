﻿using UnityEngine;

public class Planet : MonoBehaviour
{
    public GameObject Text;

    void Start()
    {
        var objectsInRange = Physics.OverlapSphere(transform.position, GetComponent<SphereCollider>().radius);
        foreach (var obj in objectsInRange){
            if (obj.tag.Equals("Player"))
                Text.SetActive(true);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("Player")){
            Text.SetActive(true);
            other.GetComponent<ShipController>().UnloadCargo();
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag.Equals("Player"))
            Text.SetActive(false);
            // add pointer to HUD
    }
}
